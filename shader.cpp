#include "shader.h"
#include <fstream>

Shader::Shader(const std::string& fileName)
{
	std::string vertexShader = LoadShader(fileName + ".vs");
	std::string fragmentShader = LoadShader(fileName + ".fs");
	/*std::string vertexShader = "#version 330 core\n"
		"layout (location = 0) in vec3 position;\n"
		"void main()\n"
		"{\n"
		"	gl_Position = vec4(position.x, position.y, position.z, 1.0);\n"
		"}\0";
	std::string fragmentShader = "#version 330 core\n"
		"out vec4 FragColor;\n"
		"void main()\n"
		"{\n"
		"	FragColor = vec4(1.0f ,0.5f, 0.2f, 1.0f);\n"
		"}\0";
	*/

	// Create each shader
	m_shaders[VERTEX_SHADER] = CreateShader(vertexShader, GL_VERTEX_SHADER);
	m_shaders[FRAGMENT_SHADER] = CreateShader(fragmentShader, GL_FRAGMENT_SHADER);

	// Create program
	m_program = glCreateProgram();
	
	glBindAttribLocation(m_program, 0, "position");
	// Attach shaders
	for (int i = 0; i < NUM_SHADERS; i++)
		glAttachShader(m_program, m_shaders[i]);
	// Link and Validate program
	glLinkProgram(m_program);
	CheckShaderError(true, GL_LINK_STATUS, m_program, "Error in linking program ");

	glValidateProgram(m_program);
	CheckShaderError(true, GL_VALIDATE_STATUS, m_program, "Error in validating program ");
	for (int i = 0; i < NUM_SHADERS; i++)
		glDeleteShader(m_shaders[i]);

}

Shader::~Shader()
{
	glDeleteProgram(m_program);
}

std::string Shader::LoadShader(const std::string& fileName)
{
	std::ifstream file;
	file.open(fileName.c_str());

	std::string output;
	std::string line;

	if(file.is_open())
	{
		while(file.good())
		{
			getline(file, line);
			output.append(line + "\n");
		}
	}
	else
	{
		std::cerr << "Could not open shader file " << fileName << ". Please check the shader file.";
		return "";
	}
	return output;

}

void Shader::Bind()
{
	glUseProgram(m_program);
}

GLuint Shader::CreateShader(std::string& text, GLuint type)
{
	GLuint shader = glCreateShader(type);
	if (shader == 0)
		std::cerr << "Shader creation failed" << std::endl;
	
	const GLchar* p[1];
	p[0] = text.c_str();
	GLint lengths[1];
	lengths[0] = text.length();


	glShaderSource(shader, 1, p, lengths);
	glCompileShader(shader);
	CheckShaderError(false, GL_COMPILE_STATUS, shader, "Shader compilation failed ");
	return shader;
}

void Shader::CheckShaderError(bool isProgram, GLuint flag, GLuint shader, const std::string& errorMessage)
{
	GLint success = 0;
	GLchar msg[1024] = { 0 };

	if (isProgram)
		glGetProgramiv(shader, flag, &success);
	else
		glGetShaderiv(shader, flag, &success);

	if (success == GL_FALSE)
	{
		if (isProgram)
			glGetProgramInfoLog(shader, sizeof(msg), NULL, msg);
		else
			glGetShaderInfoLog(shader, sizeof(msg), NULL, msg);
		std::cerr << errorMessage << ": " << msg << std::endl;
	}


}
