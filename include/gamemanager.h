#include <glad/glad.h>
#include "gameobject.h"

// Implementing in header because this is a simple class (so far)
class GameManager
{
	enum
	{
		PADDLE_1,
		PADDLE_2,
		BALL,
		NUM_GO
	};
	GameObject *gameObjects[NUM_GO];
	const float speed = 20;

	static GameManager *g;

	GameManager() {
		gameObjects[0] = NULL;
		gameObjects[1] = NULL;
		gameObjects[2] = NULL;
	};

	public:
		static GameManager* GetInstance()
		{
			if (!g)
				g = new GameManager;
			return g;
		}

		void AddGameObject(GameObject *go, int index)
		{
			gameObjects[index] = go;
		}

		void CheckBallCollision(glm::vec2& direction)
		{
			glm::vec2 ballCenter = gameObjects[BALL]->GetCenter();
			glm::vec2 paddleCenter = gameObjects[PADDLE_1]->GetCenter();

			glm::vec2 ballDimension = gameObjects[BALL]->GetDimensions();
			glm::vec2 paddleDimension = gameObjects[PADDLE_1]->GetDimensions();
			
			// Left Paddle
			if (ballCenter.x - (ballDimension.x / 2) <= paddleCenter.x + (paddleDimension.x / 2))
				if (((ballCenter.y - (ballDimension.y / 2) < paddleCenter.y + (paddleDimension.y / 2)))
					&& ((ballCenter.y + (ballDimension.y / 2) > paddleCenter.y - (paddleDimension.y / 2))))
					direction.x = -direction.x;

			// Right Paddle
			paddleCenter = gameObjects[PADDLE_2]->GetCenter();
			paddleDimension = gameObjects[PADDLE_2]->GetDimensions();

			
			if (ballCenter.x + (ballDimension.x / 2) >= paddleCenter.x - (paddleDimension.x / 2))
				if (((ballCenter.y - (ballDimension.y / 2) < paddleCenter.y + (paddleDimension.y / 2)))
					&& ((ballCenter.y + (ballDimension.y / 2) > paddleCenter.y - (paddleDimension.y / 2))))
					direction.x = -direction.x;



		}

		void Draw()
		{
			for (int i = 0; i < NUM_GO; i++)
			{
				gameObjects[i]->Draw();
			}
		}
};
GameManager *GameManager::g = 0;
