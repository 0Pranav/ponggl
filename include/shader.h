#pragma once
#include <string>
#include <iostream>
#include "glad/glad.h"
#include "GLFW/glfw3.h"

class Shader
{
public:
	Shader(const std::string& fileName);
	~Shader();
	std::string LoadShader(const std::string& fileName);
	void CheckShaderError(bool isProgram, GLuint flag, GLuint shader, const std::string& errorMessage);
	void Bind();
	GLuint CreateShader(std::string& text, GLuint type);

	GLuint GetProgram() { return m_program; };
private:
protected:
	GLuint m_program;
	enum
	{
		VERTEX_SHADER,
		FRAGMENT_SHADER,
		NUM_SHADERS
	};
	GLuint m_shaders[NUM_SHADERS];
};

