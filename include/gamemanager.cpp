#include<GLFW/glfw3.h>
#define GAME_OBJECT_COUNT 3

class GameManager
{
	public:
		GameManager();
		~GameManager();
		void InitialiseWindow();
	private:
		void gameObject[GAME_OBJECT_COUNT];
		GLFWwindow* window;
		void camera;
		
}
