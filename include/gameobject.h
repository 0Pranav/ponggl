#include "shader.h"
#include "glad/glad.h"
#include <glm/glm.hpp>

class GameObject
{
	// OpenGL stuff
	Shader *m_shader;
	GLuint vao;
	enum {
		POSITION_VB,
		INDEX_BUFFER,
		NUM_BUFFERS
	};
	GLuint vbos[NUM_BUFFERS];
	glm::vec2 vertices[4];
	unsigned int indices[6] { 0, 1, 2, 1, 2, 3 };

	// Game logic stff
	glm::vec2 center;
	float width;
	float height;

	void CalculateVertices();
	void BufferVertices();
	void AddShader(const std::string& fileName = "res/basic");
	
	public:
		GameObject(glm::vec2 center, float width, float height);
//		~GameObject();
		void Draw();
		inline glm::vec2 GetCenter() { return center; };
		inline glm::vec2 GetDimensions() { return glm::vec2(width, height); };
		void Move(const glm::vec2 displacement, bool isBall=false);
		bool IsOutOfBoundsY();
		bool IsOutOfBoundsX();
		void Center();
};
