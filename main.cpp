#include "glad/glad.h"
#include <GLFW/glfw3.h>
#include <iostream>
#include "gamemanager.h"

void ProcessInput(GLFWwindow *window, GameObject *paddle1, GameObject *paddle2);
void BallMovement(GameObject* ball, glm::vec2& direction);

int main()
{
	// Initialise OpenGL and stuff
	if(!glfwInit())
	{
		std::cerr << "GLFW Initialisation failed" << std::endl;
		return EXIT_FAILURE;
	}
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	GLFWwindow *window = glfwCreateWindow(800, 600, "PONG!", NULL, NULL);
	if(!window)
	{
		std::cerr << "Window Initialisation failed" << std::endl;
		glfwTerminate();
		return EXIT_FAILURE;
	}
	glfwMakeContextCurrent(window);
	gladLoadGL();
	
	// Create game objects
	GameObject paddle2(glm::vec2(0.9f, 0.0f), 0.1f, 0.5f);
	GameObject paddle1(glm::vec2(-0.9f, 0.0f), 0.1f, 0.5f);
	GameObject ball(glm::vec2(0.0f, 0.0f), 0.05f, 0.05f);

	GameManager *gameManager = GameManager::GetInstance();
	gameManager->AddGameObject(&paddle1, 0);
	gameManager->AddGameObject(&paddle2, 1);
	gameManager->AddGameObject(&ball, 2);

	glm::vec2 ballDirection = glm::vec2(-0.2, 1.0);

	while (!glfwWindowShouldClose(window))
	{
			glClear(GL_COLOR_BUFFER_BIT);
			ProcessInput(window, &paddle1, &paddle2);
			BallMovement(&ball, ballDirection);
			gameManager->Draw();
			glfwSwapBuffers(window);
			glfwPollEvents();
	}

	glfwDestroyWindow(window);
	glfwTerminate();
}

void ProcessInput(GLFWwindow *window, GameObject *paddle1, GameObject *paddle2)
{
		const float SPEED = 0.025;
		if(glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
			paddle1->Move(SPEED * glm::vec2(0.0, 1.0));
		if(glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
			paddle1->Move(SPEED * glm::vec2(0.0, -1.0));
		if(glfwGetKey(window, GLFW_KEY_UP ) == GLFW_PRESS)
			paddle2->Move(SPEED * glm::vec2(0.0, 1.0));
		if(glfwGetKey(window, GLFW_KEY_DOWN) == GLFW_PRESS)
			paddle2->Move(SPEED * glm::vec2(0.0, -1.0));

}

void BallMovement(GameObject* ball, glm::vec2& direction)
{
	const float speed = 0.025;
	ball->Move(speed * direction);
	if (ball->IsOutOfBoundsY())
		direction.y = - direction.y;
	if (ball->IsOutOfBoundsX())
		ball->Center();
	GameManager::GetInstance()->CheckBallCollision(direction);
}
