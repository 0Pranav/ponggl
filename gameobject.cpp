#include "gameobject.h"

GameObject::GameObject(glm::vec2 center, float width, float height)
{
	this->center = center;
	this->width = width;
	this->height = height;
	CalculateVertices();
	BufferVertices();
	AddShader();
}


void GameObject::CalculateVertices()
{
	vertices[0] = glm::vec2(center.x - width/2 ,center.y - height/2);
	vertices[1] = glm::vec2(center.x - width/2 ,center.y + height/2);
	vertices[2] = glm::vec2(center.x + width/2 ,center.y - height/2);
	vertices[3] = glm::vec2(center.x + width/2 ,center.y + height/2);
}

void GameObject::BufferVertices()
{
	glGenVertexArrays(1,&vao);
	glBindVertexArray(vao);

	// POSITION VBO
	glGenBuffers(NUM_BUFFERS, vbos);
	glBindBuffer(GL_ARRAY_BUFFER, vbos[POSITION_VB]);
	glBufferData(GL_ARRAY_BUFFER, 4 * sizeof(glm::vec2), &vertices[0], GL_STATIC_DRAW);
	glVertexAttribPointer(POSITION_VB, 2, GL_FLOAT, GL_FALSE, 0, (void*)0);
	glEnableVertexAttribArray(POSITION_VB);

	//INDEX BUFFER
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbos[INDEX_BUFFER]);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, 6 * sizeof(unsigned int), &indices[0], GL_STATIC_DRAW);

	glBindVertexArray(0);
}

void GameObject::AddShader(const std::string& fileName)
{
	m_shader = new Shader(fileName);
}

void GameObject::Draw()
{
	m_shader->Bind();
	glBindVertexArray(vao);
	glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
	glBindVertexArray(0);
}

bool GameObject::IsOutOfBoundsY()
{
	return ((center.y + height/2 >= 1) || (center.y - height/2 <= -1));
}

bool GameObject::IsOutOfBoundsX()
{
	return ((center.x + width/2 >= 1) || (center.x - width/2 <= -1));
}

void GameObject::Center()
{
	center.x = 0;
	center.y = 0;
	CalculateVertices();
	BufferVertices();
}

void GameObject::Move(glm::vec2 displacement, bool isBall)
{
	center += displacement;
	
		if (center.y + height/2 >= 1)
			center.y = 1 - height/2;
		if (center.y - height/2 <= -1)
			center.y = -1 + height/2;
	
	CalculateVertices();
	BufferVertices();
}
