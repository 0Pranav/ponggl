CC=g++
LDFLAGS =-ldl -lGL -lglfw

main: main.cpp shader.cpp gameobject.cpp glad.c 
	$(CC) -o $@ $^ -I ./include/ $(LDFLAGS)
debug: main.cpp shader.cpp gameobject.cpp glad.c 
	$(CC) -o $@ $^ -g -I ./include/ $(LDFLAGS)
clean:
	rm -f main
	rm -f debug
